import { Component } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrl: './parent.component.scss',
})
export class ParentComponent {
  isChildDestroyed = false;

  userName = 'Maria';

  ngOnInit(): void {
    console.log('ngOnInit from the parent component');
  }

  updateUser() {
    this.userName = 'Victor';
  }

  destroy() {
    this.isChildDestroyed = true;
  }
}
